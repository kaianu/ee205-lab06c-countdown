///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   ./countdown
//   Reference time: Tue Jan 21 04:26:07 PM HST 2014
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 57
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 58
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 59
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 0
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 1
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 2
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 3 
//
// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
// @date   02/24/2022
//
// Calculation used for conversion
// https://www.jotform.com/help/443-mastering-date-and-time-calculation/
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

int main() {

   time_t now;
   struct tm * timeinfo;
   struct tm info;
   char buffer[80];
   int year, month, day;
   double seconds, minutes, hours, days, months, years = 0;
   
   //referance time (my birthday) edit this time to change reference time
   year = 2022;
   month = 5;
   day = 5;

   info.tm_year = year - 1900;
   info.tm_mon = month - 1;
   info.tm_mday = day;
   info.tm_hour = 13;
   info.tm_min = 34;
   info.tm_sec = 0;
   info.tm_isdst = -1;

   int ref = mktime (&info);

   time ( &now );
   timeinfo = localtime ( &now );
   
   strftime(buffer, sizeof(buffer), "%c", &info );
   printf ("Reference time : %s HST\n", buffer);

   while(1){
   seconds = difftime(ref, now);

   //year conversion
   while (seconds >= 31556926){
      years++;
      seconds = seconds - 31556926;
   }
   //printf("%.0lf\n", years);
   
   //month conversion
   while (seconds >= 2629743){
      months++;
      seconds = seconds - 2629743;
   }
   //printf("%.0lf\n", months);

   //day conversion
   while (seconds >= 86400){
      days++;
      seconds = seconds - 86400;
   }
   //printf("%.0lf\n", days);

   //hours conversion
   while (seconds >= 3600){
      hours++;
      seconds = seconds - 3600;
   }
   //printf("%.0lf\n", hours);

   //minutes conversion
   while (seconds >= 60){
      minutes++;
      seconds = seconds - 60;
   }
   //printf("%.0lf\n", minutes);
   
   //printf("%.0lf\n", seconds);
   
   printf("Years: %.0lf\tMonths: %.0lf\tDays: %.0lf\tHours: %.0lf\tMinutes: %.0lf\tSeconds: %.0lf\n", years, months, days, hours, minutes, seconds);

   sleep(1);

   time ( &now );
   timeinfo = localtime ( &now );
   seconds = difftime(ref, now);
   
   minutes = 0;
   hours = 0;
   days = 0;
   months = 0;
   years = 0;
}


   return 0;
}
